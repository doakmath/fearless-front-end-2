function createCard(name, description, pictureUrl, startDate, endDate, confName) {
    return `
    <div class="col">
      <div class="card shadow mt-.5">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${confName}</h6>
            <p class="card-text">${description}</p>
        </div>
            <div class="card-footer">
                ${startDate} - ${endDate}
            </div>

      </div>
    </div>
    `;
  }


  function alert(message, type){
    
  }


// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';


//     try {
//       const response = await fetch(url);


//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();

//         for (let conference of data.conferences){
//             const conference = data.conferences[0];
//             const nameTag = document.querySelector('.card-title');
//             nameTag.innerHTML = conference.name;

//             const detailUrl = `http://localhost:8000${conference.href}`;
//             const detailResponse = await fetch(detailUrl);
//             if (detailResponse.ok) {
//                 const details = await detailResponse.json();
//                 // const title = details.conference.title
//                 const detailTag = document.querySelector('.card-text');
//                 detailTag.innerHTML = details.conference.description;
//                 const imageTag = document.querySelector('.card-img-top')
//                 console.log(details)
//                 imageTag.src = details.conference.location.picture_url;
//             }
//         }
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

// })




window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            // console.log(details)
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toLocaleDateString()
            const endDate = new Date(details.conference.ends).toLocaleDateString()
            const confName = details.conference.location.name
            const html = createCard(name, description, pictureUrl, startDate, endDate, confName);
            const row = document.querySelector('.row');
            row.innerHTML += html;
            // console.log(html);

          }
        }

      }
    } catch (e) {
        console.error(e);
    }

  });
